extends Node


# Declare member variables here. Examples:
var network = NetworkedMultiplayerENet.new()
var port = 1911
var max_servers = 5


# Called when the node enters the scene tree for the first time.
func _ready():
	startServer()


# Called when the server is started
func startServer():
	# setup the server using port and max_players
	network.create_server(port, max_servers)
	# ???
	get_tree().set_network_peer(network)
	
	print("Authentication server started")
	
	# attach events that get called whenever a user connects or disconnects
	#  to the server.
	network.connect("peer_connected", self, "_peer_connected")
	network.connect("peer_disconnected", self, "_peer_disconnected")

# Called whenever a player connects to the server
func _peer_connected(gateway_id):
	print("Gateway " + str(gateway_id) + " connected")

# Called whenever a player disconnects from the server
func _peer_disconnected(gateway_id):
	print("Gateway " + str(gateway_id) + " disconnected")

# Remote function to authenticate the users
remote func authenticatePlayer(username, password, player_id):
	print("Authentication request received")
	var gateway_id = get_tree().get_rpc_sender_id()
	var result = false
	print("Starting authentication")
	# for testing
	if username == "username" and password == "password":
		result = true
	# from tutorial
#	if not PlayerData.PlayerIDs.has(username):
#		print("User not recognized")
#		result = false
#	elif not PlayerData.PlayerIDs[username].Password == password:
#		print("Incorrect password")
#		result = false
#	else:
#		print("Succesful authentication")
#		result = true
	print("Authentication result send to gateway server")
	rpc_id(gateway_id, "authenticationResult", result, player_id)


















